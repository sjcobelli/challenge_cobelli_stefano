# The Challange!
### Stefano Cobelli

To install: modules can be imported with `from Challenge_Cobelli_Stefano import *`

This can be seen in the test file `test.py`, which contains unit tests. Run the file with `python3 test.py`