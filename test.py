from Challenge_Cobelli_Stefano import *


def unitTest1() :
    contact = ContactInfo("stef",  "324234234", "dog@b.com")
    assert contact.getName() is  "stef"
    assert contact.getPhoneNumber() is "324234234"
    assert contact.getEmailAddress() is  "dog@b.com"

def unitTest2() :
    #phone
    assert BuisnessCardParser.parsePhoneNumber("4105551234") == "4105551234"
    assert BuisnessCardParser.parsePhoneNumber("410-555-1234") == "4105551234"
    assert BuisnessCardParser.parsePhoneNumber("(410)555-1234") == "4105551234"
    assert BuisnessCardParser.parsePhoneNumber("Tel: +1 (703) 555-1259") == "17035551259"
    assert BuisnessCardParser.parsePhoneNumber("Dog") == ""

def unitTest3() :
    #email
    assert BuisnessCardParser.parseEmailAddress("my email is dog@cool.com") == "dog@cool.com"
    assert BuisnessCardParser.parseEmailAddress("my friends @ the club's email is yo.dude@what.gov") == "yo.dude@what.gov"
    assert BuisnessCardParser.parseEmailAddress("lets.go to.the beach@noon. ") == ""

def unitTest4() :
    #name 
    assert BuisnessCardParser.parseName("Entegra Systems\nBob Hope") == "Bob Hope"
    assert BuisnessCardParser.parseName("Hello \nRob Bope\nJunior", email="rob.bope@fun.com") == "Rob Bope"
    assert BuisnessCardParser.parseName("Hello Again\nRob Bope\nSenior", email="rbope@fun.com") == "Rob Bope"
    assert BuisnessCardParser.parseName("Bob Hope's son:\nRob Bope") == "Rob Bope"
    #fail  case
    assert BuisnessCardParser.parseName("Bob Hope\nCool Guy\nYa Know", email="dog@fun.com") == "Ya Know"
def exampleTest() :
    print('''Example 1:

Entegra Systems
John Doe
Senior Software Engineer
(410)555-1234
john.doe@entegrasystems.com

Expected Answer:

Name: John Doe
Phone: 4105551234
Email: john.doe@entegrasystems.com

Computed Result:
''')

    contact = BuisnessCardParser.getContactInfo('''Entegra Systems
John Doe
Senior Software Engineer
(410)555-1234
john.doe@entegrasystems.com''')

    print(contact)
    print("====================")
    print('''Example 2:

Acme Technologies
Analytic Developer
Jane Doe
1234 Roadrunner Way
Columbia, MD 12345
Phone: 410-555-1234
Fax: 410-555-4321
Jane.doe@acmetech.com

Expected Result:

Name: Jane Doe
Phone: 4105551234
Email: jane.doe@acmetech.com

Computer Result: 
''')
    contact = BuisnessCardParser.getContactInfo('''Acme Technologies
Analytic Developer
Jane Doe
1234 Roadrunner Way
Columbia, MD 12345
Phone: 410-555-1234
Fax: 410-555-4321
Jane.doe@acmetech.com''')
    print(contact)
    print("====================")
    print('''Example 3:

Bob Smith
Software Engineer
Decision & Security Technologies
ABC Technologies
123 North 11th Street
Suite 229
Arlington, VA 22209
Tel: +1 (703) 555-1259
Fax: +1 (703) 555-1200
bsmith@abctech.com

==>

Name: Bob Smith
Phone: 17035551259
Email: bsmith@abctech.com

Computed Answer:
    ''')
    contact = BuisnessCardParser.getContactInfo('''Bob Smith
Software Engineer
Decision & Security Technologies
ABC Technologies
123 North 11th Street
Suite 229
Arlington, VA 22209
Tel: +1 (703) 555-1259
Fax: +1 (703) 555-1200
bsmith@abctech.com''')
    print(contact)

unitTest1()
unitTest2()
unitTest3()
unitTest4()
exampleTest()