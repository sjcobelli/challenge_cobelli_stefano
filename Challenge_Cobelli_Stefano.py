import re
class ContactInfo :
    """
    Basic contact information. Contacts are nonmutable
    """
    def __init__(self, name, phone, email) :
        """
        Constructor
            :param name:String Name of contact 
            :param phone:String Phone number (unformatted) of contact
            :param email:String Email of contact
        """   
        self.__name = name
        self.__phone = phone
        self.__email = email
    def getName(self) :
        """ Gets name of contact """
        return self.__name
    def getPhoneNumber(self) :
        """ Gets phone number (unformatted) of contact """
        return self.__phone
    def getEmailAddress(self) :
        """ Gets email of contact """
        return self.__email

    def __str__(self) :
        ''' toString '''
        #str.format(), overkill here but just trying to make it not sloppy
        #https://docs.python.org/2/library/stdtypes.html#str.format
        return "Name: {name}\nPhone: {phone}\nEmail: {email}" \
        .format(name=self.__name, phone=self.__phone, email=self.__email)

class BuisnessCardParser :
    """
    Parses OCR results, all methods static
    :method: getContactInfo(document:String):ContactInfo static
    :method: parsePhoneNumber(document:String):String static
    :method: parseName(document:String):String static
    :method: parseEmailAddress(document:String):String static
    """
    @staticmethod
    def getContactInfo(document) :
        """
        Returns parsed contact information given OCR results.
        ASSUMPTIONS:
            OCR can differentiate horizontal areas of card and seperate accordingly
                eg: you'll never see a an email and phone number on the same line
            Phone numbers are assumed to be american (+1) if
                country code is not included (assuming american 
                company with american cards basically)
            OCR reads top down and the most relevent phone number 
                is given first (to not store fax number)
            Phone numbers don't have to include area codes, I guess
            A name does not contain numbers (googled it,
                actually legal in hawaii), symbols that aren't a hyphen, apostrophe,
                or space, or words from a blacklist (corperation)
            Currently names only use US alphabet eg: no umlauts or accented letters

        :param document:String The OCR results
        :returns contact:ContactInfo
        """   
        phone = BuisnessCardParser.parsePhoneNumber(document)
        email = BuisnessCardParser.parseEmailAddress(document)
        name = BuisnessCardParser.parseName(document, email=email)
        return ContactInfo(name, phone, email)

    @staticmethod
    def parsePhoneNumber(document) :
        """
        Parses phone number from OCR results

        Regex is tough, so to simplify for maintenence, I will be keeping it as simple as possible
        extra characters removed, like dots and hyphens, easier for parsing
        end cases: +1 (703) 555-1259, 1.222.333.4444, 224-2323
        become 17035551259, 12223334444, 2242323.
        TODO: add functionality for Tel/Fax differentiation, now just assuming tel is first
            :param document:String OCR results
        """   
        # If intrested in what regex string would be, here's my crack at it:
        # (?:\+?\d{1,2})?[-. (]*\d{3}[-. )]*\d{3}[-. ]*\d{4}$
        # country code: 1 or 2 digits, may include plus
        # area code: 3 numbers, might be surounded by parens or '-. '
        # the rest: 3 numbers then 4 numbers with possible '- .' in between
        # should be new line in most casses at end with ideal OCR, I couldn't think of cases that didn't
        # End of line also helps with 11/12 digit numbers with no seperators
        #   works for (410)555-1234, 4105551234, 410-555-1234, Tel: +1 (703) 555-1259, 17035551259

        #remove inbetween characters
        for c in ' -().' :
            if c in document :
                document = document.replace(c, "")
        #re.search gets first occurance of match
        #^\d{7,}? is any \digit 7 times in a row or more
        match = re.search(r'\b\d{7,}?\b', document)
        if match: 
            return match.group()
        else :
            return ""

    @staticmethod
    def parseName(document, email="") :
        
        """
        Parses Name from OCR results. Judges based on blacklist, if name is in email address, 
        or if a string has weird characters
            :param document:  OCR results
            :param email="": parsed email
        """
        lines = document.split('\n')
        # Index of the line that does not violate the rules
        goodIndex = 0
        # restricted name words, arbitrary
        blacklist = ['Collective', 'Enterprises', 'Group',
        'Industries', 'International', 'Services', 'Systems',
        'Software', 'LLC', 'Corp', 'Inc', 'INC'] # Case sensitive
        # Takes the first half of the email address (the name part... probably a name for this)
        # Regex string is all characters before '@'
        firstAddressPart =""
        if email :
            firstAddressPart = re.search(r'.+(?=@)', email)
            if firstAddressPart == None: return ""
            firstAddressPart= firstAddressPart.group()
        for line in lines :
            # contains a number or symbol
            # regex is opposite of valid characters
            if bool(re.search(r'^[^A-Za-z\' -]$', line)) :
                continue 
            
            # for each word in the line of the card, check if it in the name part of the email
            # Example: Doug Smith would work for dsmith@gmail.com because it contains smith
            
            wordList = line.replace(".", "").split(" ")
            for badWord in blacklist :
                if badWord in wordList :
                    continue

            for word in wordList :
                if len(word) > 2 and word.lower() in firstAddressPart.lower() :
                    return line #return name if part of name is in first part of email
            
            goodIndex = lines.index(line)

        # No name has been chosen
        # Default to first line that doesn't violate rules, nothing else to do
        return lines[goodIndex]

    @staticmethod
    def parseEmailAddress(document) :
        """
        Parses email from OCR results
            :param document:String OCR results
        """  
        # regex: email is one 'word' so can use \b anchors
        #   any valid characters, including '.', up to '@'
        #   any calid characters, so long as there's a '.' in there
        match = re.search(r'\b[\w.%+-]+@[\w-]+\.[\w.-]+\b', document)
        if match:
            return match.group()
        else :
            return ""
    
